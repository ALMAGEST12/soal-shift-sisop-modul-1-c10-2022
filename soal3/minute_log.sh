#!/bin/bash

file="metrics_"$(date "+%Y%m%d%H%M%S")".log"
touch metrics_"$(date "+%Y%m%d%H%M%S")".log

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> ./log/$file

#ram
free -m > ram.log
awk 'FNR==2{printf $2","$3","$4","$5","$6","$7","}''FNR==3{printf $2","$3","$4","}' ram.log >> ./log/$file

#user
user=`pwd`
echo -n $user >> ./log/$file

#disk
du -sh $HOME  > du.log
awk '{print ","$1}' du.log >> ./log/$file

chmod 400 ~/log/$file

