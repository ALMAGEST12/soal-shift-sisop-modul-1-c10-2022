#!/bin/bash

perHour=$(date -d "$x - 1 hours" "+%Y%m%d%H")

chmod -R 777 /home/$USER/log/

awk -F"," '
BEGIN{OFS=",";MIN1=100000;MIN2=100000;MIN3=100000;MIN4=100000;MIN5=100000;MIN6=100000;MIN7=100000;MIN8=100000;MIN9=100000;MIN11=100000}
	$1<MIN1 {MIN1=$1;} $2<MIN2 {MIN2=$2;} $3<MIN3 {MIN3=$3;} $4<MIN4 {MIN4=$4;} $5<MIN5 {MIN5=$5;} $6<MIN6 {MIN6=$6;} $7<MIN7 {MIN7=$7;} $8<MIN8 {MIN8=$8;} $9<MIN9 {MIN9=$9;} $11<MIN11 {MIN11=$11;}
END{print "minimum",MIN1,MIN2,MIN3,MIN4,MIN5,MIN6,MIN7,MIN8,MIN9,$10,MIN11}' ~/log/metrics_$perHour*.log > temp.log

awk -F"," '
BEGIN{OFS=",";MAX1=0;MAX2=0;MAX3=0;MAX4=0;MAX5=0;MAX6=0;MAX7=0;MAX8=0;MAX9=0;MAX1=0}
        $1>MAX1 && FNR%2==0{MAX1=$1;} $2>MAX2 && FNR%2==0{MAX2=$2;} $3>MAX3 && FNR%2==0{MAX3=$3;} $4>MAX4 &&FNR%2==0 {MAX4=$4;} $5>MAX5 && FNR%2==0{MAX5=$5;} $6>MAX6 && FNR%2==0{MAX6=$6;} $7>MAX7 &&FNR%2==0{MAX7=$7;} $8>MAX8 && FNR%2==0{MAX8=$8;} $9>MAX9 &&FNR%2==0{MAX9=$9;} $11>MAX11 && FNR%2==0{MAX11=$11;}
END{print "maximum",MAX1,MAX2,MAX3,MAX4,MAX5,MAX6,MAX7,MAX8,MAX9,$10,MAX11}' ~/log/metrics_$perHour*.log >> temp.log

awk -F"," '
BEGIN{OFS=",";A=0;B=0;C=0;D=0;E=0;F=0;G=0;H=0;I=0;K=0}
	{A+=$1;B+=$2;C+=$3;D+=$4;E+=$5;F+=$6;G+=$7;H+=$8;I+=$9;K+=$11}
END{print "average",A/NR,B/NR,C/NR,D/NR,E/NR,F/NR,G/NR,H/NR,I/NR,$10,K/NR"G"}' ~/log/metrics_$perHour*.log >> temp.log

string="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available_swap_used,swap_free,path,path_size"

echo $string > /home/sarah/log/metrics_agg_$perHour.log && cat /home/sarah/temp.log >> /home/sarah/log/metrics_agg_$perHour.log

chmod 400 /home/$USER/log/metrics_agg_$perHour.log






