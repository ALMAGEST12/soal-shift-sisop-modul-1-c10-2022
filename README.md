# soal-shift-sisop-modul-1-C10-2022
Pembahasan soal praktikum Modul 1 Sistem Operasi 2022

**Anggota Kelompok**:

- Sarah Alissa Putri - 5025201272
- Monica Narda Davita - 5025201009
- William Zefanya Maranatha - 5025201167

---

# Daftar Isi
- [Soal 1](#soal-1)
    - [Soal 1.a](#soal-1a)
    - [Soal 1.b](#soal-1b)
    - [Soal 1.c](#soal-1c)
    - [Soal 1.d](#soal-1d)
- [Soal 3](#soal-3)

## Soal 1
**[Source Code Soal 1](https://gitlab.com/MonicaDavita/soal-shift-sisop-modul-1-c10-2022/-/tree/main/soal1)**

**Deskripsi:**\
Membuat sistem register dan login untuk mengunduh N gambar dan menampilkan banyaknya percobaan login oleh tiap user

### Soal 1.a
**Deskripsi:**\
Membuat sistem register pada script `register.sh`, sistem login pada `main.sh`, dan file `user.txt` yang berada pada folder ./users

**Pembahasan:**
- Membuat folder users pada root (home), mengubah lokais direktori, dan membuat file .txt yang diinginkan
```bash
mkdir users
touch user.txt
```

- Menginputkan username dan password dengan `read`
- Memeriksa apakah username sudah ada pada file `user.txt` atau belum
```bash
cd users
grep -qF "$username" user.txt
```
Perlu mengganti posisi saat ini ke direktori users agar dapat memerika file `user.txt`. Untuk memeriksa nama username, maka menggunakan fungsi `grep -qF`.

- Bila username sudah berada pada pada file `user.txt` maka tampilkan perintah agar user melakukan login 
-Bila username belum terdata, maka `read password` dan masukkan username dan password ke `user.txt`
```bash
echo "Username: ${username}" >> user.txt
echo "Password: ${password}" >> user.txt
```
**Bukti:**
- Bukti 1.a registrasi berhasil, login berhasil, dan login gagal
![BuktiRegisterSukses](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20a/1a%20register%20bukti%20sukses.jpg)
![BuktiLoginnSuksesGagal](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20a/1a%20main%20bukti%20login%20sukses%20dan%20gagal.jpg)

**Kendala:**\
Kesulitan dalam memahami fungsi `grep -qF`

### Soal 1.b
**Deskripsi:**\
Password ketika diketikkan harus bersifat hidden dan memiliki kriteria: tidak boleh kurang dari 8 karakter, memiliki minimal satu huruf besar dan kecil, bersifat alphanumeric, dan tidak boleh sama dengan username.

**Pembahasan:**
- Input password dapat disembunyikan dengan menggunakan `s`
```bash
read -s password
```
- Membentuk fungsi `checkLength` untuk mengecek panjang password. Menggunakan operator `-ge` (greater and equals)
```bash
function checkLength(){
	if [ "${#password}" -ge 8 ]; then
		checkUsername			
	else				
		echo "Password must contain minimum 8 characters!"
	fi
}
```
- Bila password melebihi 8 karakter, maka lanjut mengecek apakah password sama atau tidak dengan username
```bash
function checkUsername(){
	if [ "$password" != "$username" ]; then	
		checkChar					
	else								
		echo "Password must not the same as the username!"
	fi
}
```
- Lanjut mengecek apakah password memiliki minimal satu huruf kapital dan kecil dengan fungsi `[[:lower]]` dan `[[:upper]]`
```bash
function checkChar(){
	if [[ "$password" =~ [[:lower:]] && "$password" =~ [[:upper:]] ]]; then
		checkAlphanumeric							
	else									
		echo "Password must contain upper and lower case!"
	fi
}
```
-Terakhir, memeriksa apakah password sudah bersifat alphanumeric atau belum dengan bantuan `^[a-zA-Z0-9]+$`. tanda '^' menandakan awal string sedangkan '$' menandakan akhir string.
```bash
function checkAlphanumeric(){
	if [[ "$password" =~ ^[a-zA-Z0-9]+$ ]]; then					
		echo "Username: ${username}" >> user.txt
		echo "Password: ${password}" >> user.txt
	else										
		echo "Password must be an alphanumeric string!"
	fi
}
```
**Bukti:**
- Bukti 1b salah satu syarat password tidak terpenuhi sehingga invalid dan password telah tersembunyi
![BuktiPasswordInvalid](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20b/1b%20register%20bukti%20password%20invalid.jpg)
![BuktiPasswordTersembunyi](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20b/1b%20register%20bukti%20password%20hidden%20dan%20sukses.jpg)

**Kendala:**\
Pencarian cara yang efektif dan mudah untuk mengecek syarat password yang harus merupakan alphanumeric. Terkadang ada error dalam penulisan sintaks dan sebagainya.

### Soal 1.c
**Deskripsi:**\
Mencatat usaha register dan login pada file `log.txt` yang berada pada folder users. Format yang tertulis adalah DD/MM/YYYY HH:MM:SS MESSAGES

**Pembahasan:**
- Membuat file `log.txt` pada folder users melalui terminal
```bash
cd users
touch log.txt`
```
- Membuat variabel untuk menyimpan tanggal dan waktu terkini 
```bash
time=`date +'%D %T'`
```
- Untuk memastikan apakah username sudah terdaftar sebelumnya, maka dapat digunakan fungsi `grep -qF` yang sudah tertera sebelumnya. Bila ada, maka statusnya error. Bila tidak ada, maka registrasi berhasil. Lalu memasukkan pesan status yang sesuai ke dalam log.txt
```bash
if grep -qF "$username" user.txt; then
    status="$time REGISTER: ERROR User already exist"			
	echo "$status" >> log.txt
else		
	echo "Password: "
	read -s password						
	checkLength								
	status="$time REGISTER: INFO  User $username registered successfully"	
	echo "$status" >> log.txt						
fi
```
- Begitu pula untuk Login menggunakan konsep yang sama
```bash
if grep -qF "$password" user.txt; then
    status="$time LOGIN: INFO User $username loged in"
    echo "$status" >> log.txt
else
    status="$time LOGIN: ERROR Failed login attempt on user $username"
	echo "$status" >> log.txt
fi
```
**Bukti:**
- Gagal registrasi: ![BuktiGagalRegister](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20c/1c%20register%20bukti%20poin%20i%20failed%20register.jpg)
- Registrasi berhasil: ![BuktiRegisterBerhasil](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20c/1c%20register%20bukti%20poin%20ii%20success%20register.jpg)
- Gagal login: ![BuktiLoginGagal](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20c/1c%20main%20bukti%20poin%20iii%20failed%20login.jpg)
- Gagal registrasi: ![BuktiLoginBerhasil](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20c/1c%20main%20bukti%20poin%20iv%20success%20login.jpg)

**Kendala:**\
Mencari fungsi tanggal dan waktu yang sesuai. Sisanya terkendali.

### Soal 1.d
**Deskripsi**\
Membuat dua perintah untuk mengunduh N gambar dari link yang sudah ditentukan dan menampilkan berapa banyak percobaan login dari user

**Pembahasan:**
- Bila user mengetikkan 'dl N', maka kita perlu memeriksa berapakah angka 'N' tersebut dengan bantuan `tr -dc '0-9'`
```bash
read command
number=$(echo "$command" | tr -dc '0-9')
```
- Pada kasus yang sama, bila user ingin mengunduh sebanyak N gambar, maka kita perlu memastikan apakah user sudah pernah mengunduh sebelumnya atau belum. Bila belum, maka kita perlu membuat direktori dengan ketentuan "YYYY-MM-DD_username". Format tanggal adalah `(date +'%Y-%m-%d')`. Mengunduh gambar dapat menggunakan `curl` dan untuk mengubah nama file menjadi PIC_01, PIC_02, dst.. menggunakan `-o` 
- Untuk zip folder, perlu menginstall zip terlebih dahulu pada terminal. Setelah itu, folder dapat dizip dengan password yang sesuai melalui command `-P'
```bash
mkdir "$(date +'%Y-%m-%d')_$username"
cd "$(date +'%Y-%m-%d')_$username"
for ((num=1; num<=number; num=num+1))
do
    curl https://loremflickr.com/cache/resized/65535_51729713723_aee92ee535_n_320_240_nofilter.jpg -o "PIC_0$num.jpg"
done
cd ~
zip -P "$password" -r "$(date +'%Y-%m-%d')_$username" "$(date +'%Y-%m-%d')_$username" 
```
- Bila sebelumnya user sudah mengunduh gambar, maka kita perlu unzip folder sebelumnya dan menghapus file .zip yang lalu. Di folder yang ter-unzip itu akan dilakukan perintah yang sama seperti langkah sebelumnya. Hanya saya disini perlu dicari tahu ada berapa banyak file yang ada pada folder sehingga penamaan file dapat berlanjut (PIC_0N, PIC_0N+1, ...)
```bash
num=$(ls | wc -l | tr -dc '0-9') ### mencari banyaknya file pada folder ###
```
- Untuk `att`, merupakah hasil penjumlahan dari percobaan login yang sukses dan gagal. Untuk percobaan login sukses pasti memiliki substring "...$username loged in.." pada `log.txt`
- Sedangkan untuk percobaan login gagal akan memiliki fungsi "...on user $username" pada `log.txt.` Sehingga, dapat digunakan `grep -o -i` untuk mencari kedua substring tersebut pada `log.txt`
```bash
success=$(grep -o -i "$username loged" log.txt | wc -l | tr -dc '0-9')
failed=$(grep -o -i "on user $username" log.txt | wc -l | tr -dc '0-9')
let att=success+failed
echo "$att"
```
- wc -l : untuk menghitung banyaknya substring yang diminta
- tr dc '0-9' : mengubah angka yang berupa string menjadi integer agar dapat dilakukan operasi penjumlahan

**Bukti:**
- Bukti dl N oleh user baru: ![BuktiDlBaru](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20d/1d%20main%20bukti%20dl%20N%20new%20user.jpg)
- Bukti dl N oleh user lama: ![BuktiDlLama](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20d/1d%20main%20bukti%20dl%20N%20user%20lama.jpg)
- Bukti att: ![BuktiAtt](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/SS/Poin%20d/1d%20main%20bukti%20att.jpg)

**Kendala:**
- Kesulitan dalam zip dan unzip
- Kebingungan dalam bagaimana cara menghitung banyak file dalam folder
- Kesulitan dalam memasang lock zip

## Soal 3
**minute_log.sh**

Deskripsikan variabel *file* sebagai tempat tujuan penyimpanan akhir dengan menambahkan tanggal dan jam sesuai dengan waktu file akan dijalankan. Gunakan perintah touch untuk membuat file.
```ruby
file="metrics_"$(date "+%Y%m%d%H%M%S")".log"
touch $file
```

Pindahkan catatan yang ada di *free -m* ke file log baru bernama ram.log
```ruby
free -m > ram.log
```

Ambil angka yang ada di ram.log menggunakan perintah awk, kemudian masukkan ke file tujuan.
```ruby
awk 'FNR==2{printf $2","$3","$4","$5","$6","$7","}''FNR==3{printf $2","$3","$4","}' ram.log >> ./log/$file
```
Definisikan variabel *user* untuk dimasukkan ke file yang sama.
```ruby
user=`pwd`
echo -n $user >> ./log/$file
```
Pindahkan catatan yang ada di *du -sh $HOME* ke file log baru bernama du.log. Kemudian ambil nilai yang ada di du.log menggunakan perintah awk lalu dimasukkan ke file tujuan.
```ruby
du -sh $HOME  > du.log
awk '{print ","$1}' du.log >> ./log/$file
```

Gunakan perintah chmod karena chmod 400 hanya memberikan akses baca kepada pemiliknya dan tidak ada izin lain yang diberikan kepada grup dan lainnya.
```ruby
chmod 400 ~/log/$file
```

Masukkan perintah cronjob di crontab -e agar file dapat berjalan secara automatis setiap menitnya
```ruby
* * * * * /home/sarah/minute_log.sh
```

Contoh output yang akan muncul di file:
```ruby
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15681,4901,4657,1332,6122,9123,2047,0,2047,/home/sarah,2,1G
```

**aggregate_minutes_to_hourly_log.sh (REVISI)**

Buat variable perHour untuk mengetahui dan menyimpan data satu jam yang lalu.
```ruby
perHour=$(date -d "$x - 1 hours" "+%Y%m%d%H")
```

Gunakan chmod -R 777 untuk memungkinkan SEMUA pengguna dapat izin untuk membaca, menulis, dan mengeksekusi isi file di log.
```ruby
chmod -R 777 /home/$USER/log/
```

Untuk mendapatkan nilai minimum, buat variable dengan nilai 100000 untuk membandingkan setiap nilai metrics menggunakan awk dari file yang sesuai pada jam tertentu kemudian ditampung sementara di file temp.log.
```ruby
 awk -F"," '
BEGIN{OFS=",";MIN1=100000;MIN2=100000;MIN3=100000;MIN4=100000;MIN5=100000;MIN6=100000;MIN7=100000;MIN8=100000;MIN9=100000;MIN11=100000}
        $1<MIN1 {MIN1=$1;} $2<MIN2 {MIN2=$2;} $3<MIN3 {MIN3=$3;} $4<MIN4 {MIN4=$4;} $5<MIN5 {MIN5=$5;} $6<MIN6 {MIN6=$6;} $7<MIN7 {MIN7=$7;} $8<MIN8 {MIN8=$8;} $9<MIN9 {MIN9=$9;} $11<MIN11 {MIN11=$11;}
END{print "minimum",MIN1,MIN2,MIN3,MIN4,MIN5,MIN6,MIN7,MIN8,MIN9,$10,MIN11}' ~/log/metrics_$perHour*.log > temp.log
```

Untuk mendapatkan nilai maximum, buat variable dengan nilai 0 untuk membandingkan setiap nilai metrics menggunakan awk dari file yang sesuai pada jam tertentu kemudian ditampung sementara di file temp.log. Menggunakan FNR%2==0 untuk memastikan kalau line yang dibaca hanya line 2 saja.
```ruby
 awk -F"," '
BEGIN{OFS=",";MAX1=0;MAX2=0;MAX3=0;MAX4=0;MAX5=0;MAX6=0;MAX7=0;MAX8=0;MAX9=0;MAX1=0}
        $1>MAX1 && FNR%2==0{MAX1=$1;} $2>MAX2 && FNR%2==0{MAX2=$2;} $3>MAX3 && FNR%2==0{MAX3=$3;} $4>MAX4 &&FNR%2==0 {MAX4=$4;} $5>MAX5 && FNR%2==0{MAX5=$5;} $6>MAX6 && FNR%2==0{MAX6=$6;} $7>MAX7 &&FNR%2==0{MAX7=$7;} $8>MAX8 && FNR%2==0{MAX8=$8;} $9>MAX9 &&FNR%2==0{MAX9=$9;} $11>MAX11 && FNR%2==0{MAX11=$11;}
END{print "maximum",MAX1,MAX2,MAX3,MAX4,MAX5,MAX6,MAX7,MAX8,MAX9,$10,MAX11}' ~/log/metrics_$perHour*.log >> temp.log
```

Untuk mendapatkan nilai rata-rata, buat variable dengan nilai 0 lalu nilai metrics akan dijumlahkan ke tiap variable kemudian dibagi dengan jumlah line yang ada. Nilai yang didapatkan akan ditampung sementara ke file temp.log.
```ruby
awk -F"," '
BEGIN{OFS=",";A=0;B=0;C=0;D=0;E=0;F=0;G=0;H=0;I=0;K=0}
	{A+=$1;B+=$2;C+=$3;D+=$4;E+=$5;F+=$6;G+=$7;H+=$8;I+=$9;K+=$11}
END{print "average",A/NR,B/NR,C/NR,D/NR,E/NR,F/NR,G/NR,H/NR,I/NR,$10,K/NR"G"}' ~/log/metrics_$perHour*.log >> temp.log
```

Buat variable string berisikan keterangan sesuai dari free -m dan du -sh
```ruby
string="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available_swap_used,swap_free,path,path_size"
```

Echo variable string ke file metrics_agg_$perHour.log kemudian masukkan nilai yang ada di temp.log ke file metrics_agg_$perHour.log.
```ruby
echo $string > /home/sarah/log/metrics_agg_$perHour.log && cat /home/sarah/temp.log >> /home/sarah/log/metrics_agg_$perHour.log
```

Terakhir, ubah permission menjadi chmod 400 agar file hanya dapat dibaca oleh user saja.
```ruby
chmod 400 /home/$USER/log/metrics_agg_$perHour.log
```

Tambahkan cornjob di crontab -e agar file dapat berjalan automatis setiap jamnya
```ruby
00 * * * * /home/sarah/aggregate_minutes_to_hourly_log.sh 
```

**Kendala :**
 Sempat salah dalam menjalankan cron, nilai metrics bagian maximum tidak keluar, dan satuan yang ada di du -sh tidak ikut muncul.

**Solusi :**
 - Path filenya ternyata salah sehingga file tidak dapat berjalan automatis setiap menitnya 
 - Menggunakan FNR%2==0 agar line yang dibaca hanya line 2
 - Ditambahkan secara manual menggunakan "G".
