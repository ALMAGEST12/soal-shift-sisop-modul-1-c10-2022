mkdir -p forensic_log_website_daffainfo_log

awk '
BEGIN { FS = ":" }; 
(NR > 1) {request_count++; }; 
END {
print "Rata-rata serangan adalah sebanyak " request_count/12 " requests per jam";}
' log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/ratarata.txt

awk '
BEGIN { FS = ":" }; 
(NR > 1) {ip_count[$1]++}; 
/curl/ {count_curl++};
END {
ip_max = 0;
for (i in ip_count) if(ip_count[i] > ip_count[ip_max]) ip_max = i;
print "IP yang paling banyak mengakses server adalah: "ip_max" sebanyak "ip_count[ip_max]" requests";
print "Ada " count_curl " requests yang menggunakan curl sebagai user-agent";}
' log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/result.txt	

awk -F ':' '/2022:02/ {if(ip_count2022[$1]!=1) print $1;ip_count2022[$1]=1;} ' log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
