#!/bin/bash

echo "Register (1) or login (2)?"
read input

if [ "$input" -eq 2 ]; then
	echo "Please login"
	echo "Username:"
	read username

	time=`date +'%D %T'`

	cd users
	if grep -qF "$username" user.txt; then
		echo "Password:"
		read -s password

		if grep -qF "$password" user.txt; then
            status="$time LOGIN: INFO User $username loged in"
            echo "$status" >> log.txt
			echo "1) Download N images (type 'dl N')"
			echo "2) How many attempts have you already done (type 'att')?"
			read command
			number=$(echo "$command" | tr -dc '0-9')

			if [[ "$command" == *"dl"* ]]; then
				cd ~
				if [ -f "$(date +'%Y-%m-%d')_$username.zip" ]; then
					unzip "$(date +'%Y-%m-%d')_$username"
					rm "$(date +'%Y-%m-%d')_$username.zip"
					cd "$(date +'%Y-%m-%d')_$username"
					num=$(ls | wc -l | tr -dc '0-9')
					let jumlah=num+1
					let total=number+num
				
                               		for ((no=jumlah; no<=total; no=no+1))
                               		do
                                      		curl https://loremflickr.com/cache/resized/65535_51729713723_aee92ee535_n_320_240_nofilter.jpg -o "PIC_0$no.jpg"
                               		done
					cd ~
                               		zip -P "$password" -r "$(date +'%Y-%m-%d')_$username" "$(date +'%Y-%m-%d')_$username"
                               		rm -r "$(date +'%Y-%m-%d')_$username"

				else
					cd ~
					mkdir "$(date +'%Y-%m-%d')_$username"
					cd "$(date +'%Y-%m-%d')_$username"
					for ((num=1; num<=number; num=num+1))
					do
						curl https://loremflickr.com/cache/resized/65535_51729713723_aee92ee535_n_320_240_nofilter.jpg -o "PIC_0$num.jpg"
					done
					cd ~
					zip -P "$password" -r "$(date +'%Y-%m-%d')_$username" "$(date +'%Y-%m-%d')_$username" 
					rm -r "$(date +'%Y-%m-%d')_$username"
				fi
			elif [ "$command" == "att" ]; then
				success=$(grep -o -i "$username loged" log.txt | wc -l | tr -dc '0-9')
				failed=$(grep -o -i "on user $username" log.txt | wc -l | tr -dc '0-9')
				let att=success+failed
				echo "$att"
			fi
        	else
                status="$time LOGIN: ERROR Failed login attempt on user $username"
				echo "$status" >> log.txt
        	fi

	else
		echo "Username not found! Please register first"
		cd ~
		bash register.sh
	fi
elif [ "$input" -eq 1 ]; then
	cd ~
	bash register.sh
fi