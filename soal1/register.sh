#!/bin/bash#

#### Pengantar Registrasi #####
echo "Welcome, please register first!"

#### User memasukkan username yang diinginkan ####
echo "Username: "
read username

#### Fungsi untuk mengecek panjang password ####
function checkLength(){

	if [ "${#password}" -ge 8 ]; then	#### Bila password lebih dari sama dengan 8 karakter ####
		checkUsername			#### Lanjut ke pengecekan selanjutnya, yaitu apakah password sama dengan username atau tidak ####
	
	else					#### Bila masih kurang dari 8 karakter, tampilkan pesan yang sesuai ####	
		echo "Password must contain minimum 8 characters!"
	fi
}


#### Fungsi untuk mengecek apakah password sama dengan username atau tidak ####
function checkUsername(){

	if [ "$password" != "$username" ]; then				#### Bila tidak sama, maka ####
		checkChar						#### Lanjutkan ke pengecekan password selanjutnya, yaitu apakah password memiliki huruf kapital dan kecil ####
	
	else								#### Bila sama, maka tampilkan pesan yang sesuai ####
		echo "Password must not the same as the username!"
	fi
}


#### Fungsi untuk mengecek apakah password memiliki huruf kapital dan kecil ####
function checkChar(){

	if [[ "$password" =~ [[:lower:]] && "$password" =~ [[:upper:]] ]]; then		#### Jika password memiliki huruf kecil dan juga huruf kapital, maka ####
		checkAlphanumeric														#### Cek password apakah sudah alphanumeric atau belum ####

	else										#### Bila belum memiliki huruf kecil dan besar, maka tampilkan pesan yang sesuai ####
		echo "Password must contain upper and lower case!"
	fi
}


#### Fungsi untuk mengecek apakah password sudah alphanumeric atau belum ####
function checkAlphanumeric(){

	if [[ "$password" =~ ^[a-zA-Z0-9]+$ ]]; then					#### Jika password sudah alphanumerik, maka simpan username dan password ke dalam user.txt ####
																	#### tanda ^ untuk menandakan awal lalu $ menandakan akhir dari suatu string	
		echo "Username: ${username}" >> user.txt
		echo "Password: ${password}" >> user.txt
		status="$time REGISTER: INFO  User $username registered successfully"	#### Bila berhasil, maka status berisi pesan tanggal, waktu, dan INFO yang sesuai ####
		echo "$status" >> log.txt						#### SImpan ke dalam log.txt pesan status tersebut ####

	else										#### BIla belum alphanumerik, maka tampilkan pesan yang sesuai ####
		echo "Password must be an alphanumeric string!"
	fi
}

#### Variabel yang menyatakan tanggal dan waktu saat ini ####
time=`date +'%D %T'`
cd users
	#### Bila username ditemukan pada user.txt ####
	if grep -qF "$username" user.txt; then
        status="$time REGISTER: ERROR User already exist"			#### Assign variabel status dengan pesan tanggal, waktu, dan ERROR yang sesuai ####
		echo "$status" >> log.txt						#### Simpan ke dalam log.txt ####
	
	#### Bila tidak ditemukan, maka user dapat menginput password untuk registrasi ####
	else		
		echo "Password: "
		read -s password							#### Input password secara hidden ####
		checkLength								#### Lakukan pengecekan password dimulai dari fungsi yang mengecek panjang password ####
		
	fi

#### Menampilkan pesan status yang sesuai ke user #### 
echo "$status"
